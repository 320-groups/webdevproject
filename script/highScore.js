'use strict';
  /**
  * @title score
  * @description Returns the score for the current player
  * @author Hugues Rouillard
  */
function score() {
  const color = document.querySelector("#color").value;
  const size = document.querySelector("#board-size").value;
  const diff = document.querySelector("#difficulty").value;
  
  const numCorrect = checkAnswers(color);
  const numSelected = numberSelected();
  const boardSize = parseInt(size);
  const difficulty = parseInt(diff);

  const percent = ((2 * numCorrect - numSelected) / (boardSize * boardSize));
  const scoreResult = Math.floor(percent * 100 * boardSize * (difficulty + 1));

  console.log(scoreResult);
  return scoreResult;
}
  /**
  * @title insertScoreTable
  * @description Inserts the Highscore table in the correct area
  * @author Hugues Rouillard
  */
//Creates and inserts a scoreTable for the highscore
function insertScoreTable(){
  //gets the items from the local storage if there are any
  let scoresArray = [];
  if(localStorage.getItem('scores')){
    scoresArray = JSON.parse(localStorage.getItem('scores'));
  } else{
    return;
  }
  const table = document.querySelector('#score-table');
  table.innerHTML = '';
  let thead = document.createElement('thead');
  let tbody = document.createElement('thead');
  let tr = document.createElement('tr');
  let th1 = document.createElement('th');
  th1.innerText = 'Name';
  let th2 = document.createElement('th');
  th2.innerText = 'Score';
  tr.appendChild(th1);
  tr.appendChild(th2);
  thead.appendChild(tr);
  table.appendChild(thead);
  scoresArray.forEach((usrGame) => {
    let newR = document.createElement('tr');
    let newName = document.createElement('td');
    newName.innerText = usrGame.name;
    newName.classList.add('scoretd');
    let newScore = document.createElement('td');
    newScore.innerText = usrGame.score;
    newScore.classList.add('scoretd');
    newR.appendChild(newName);
    newR.appendChild(newScore);
    tbody.appendChild(newR);
    table.appendChild(tbody);
  })
  
}
  /**
  * @title clearBoard
  * @description Cleares the highscore table
  * @author Hugues Rouillard
  */
function clearBoard(){
  const empty = [];
  localStorage.setItem('scores',JSON.stringify(empty));
  insertScoreTable();
}
  /**
  * @title addLocalStorage
  * @description Adds to the local storage
  * @param {String} name
  * @param {number} score
  * @author Vijay Patel & Hugues Rouillard
  */

function alertScore(colorChosen){
  let percentage = ((checkAnswers(colorChosen)/correctAnswersCount(colorChosen)));
  if(percentage > 0.59){
    alert(`Congrats... you've somehow... won? wow.. You have succesfully clicked ${checkAnswers(colorChosen)} tiles out of ${correctAnswersCount(colorChosen)}! Your score percentage is ${percentage*100}%`);
  } else{
    alert(`HAHAHAHAHAH I KNEW IT! No one beside me is good enough to finish this game! You have clicked ${checkAnswers(colorChosen)} tiles out of ${correctAnswersCount(colorChosen)} Your score percentage is ${percentage*100}%`);
  }
 }
//Adds to the local storage
function addLocalStorage(name, score){
  let HS = [];
  if(localStorage.getItem('scores')){
    HS = JSON.parse(localStorage.getItem('scores'));
  }

  const sortedArray = HS.toSorted((a,b) => b.score - a.score);

  if (sortedArray.length === 10) {
    if(score > sortedArray[sortedArray.length - 1].score){
      sortedArray[sortedArray.length - 1] = { name: name, score: score };
    }
  } else if (sortedArray.length <10){
    sortedArray.push({name: name, score: score});
  }
  sortedArray.sort((a,b) => b.score - a.score);
  localStorage.setItem('scores',JSON.stringify(sortedArray));
}
  /**
  * @title Reverse
  * @description unorders (from asc to desc) the info in the localStorage
  * @author Vijay Patel
  */
function reverse(){
  let HS = [];
  if(localStorage.getItem('scores')){
    HS = JSON.parse(localStorage.getItem('scores'));
  } else{
    return;
  }
  const reverseSortedArray = HS.toSorted((a,b) => a.score - b.score);
  localStorage.setItem('scores',JSON.stringify(reverseSortedArray));
}
  /**
  * @title unReverse
  * @description re-orders (from desc to asc) the info in the localStorage
  * @author Vijay Patel
  */
function unReverse(){
  let HS = [];
  if(localStorage.getItem('scores')){
    HS = JSON.parse(localStorage.getItem('scores'));
  } else{
    return;
  }
  const reverseSortedArray = HS.toSorted((a,b) => b.score - a.score);
  localStorage.setItem('scores',JSON.stringify(reverseSortedArray));
}
  /**
  * @description When mouse enters, unorders the highscore table
  * @author Vijay Patel
  */
document.querySelector("#hs").addEventListener("mouseover",function(){
  reverse();
  insertScoreTable();
});
  /**
  * @description When mouse leaves, re-orders the highscore table
  * @author Vijay Patel
  */
document.querySelector("#hs").addEventListener("mouseout",function(){
  unReverse();
  insertScoreTable();
});
