"use strict";
  /**
  * @description Shows or hides the cheat
  * @param {String} color
  * @author Hugues Rouillard & Vijay Patel
  */
  let trackVisibility = false;
  document.addEventListener('keydown', function (e) {
    let cheat = document.querySelectorAll(".rgbanswers");
    let cheat2 = document.querySelectorAll(".coloranswers");
  
    if (e.shiftKey && e.key === "C") {
      if (!trackVisibility) {
        cheat.forEach((colorCode) => colorCode.style.visibility = 'visible');
        cheat2.forEach((colorCode) => colorCode.style.visibility = 'visible');
        trackVisibility = true; 
      } else {
        cheat.forEach((colorCode) => colorCode.style.visibility = 'hidden');
        cheat2.forEach((colorCode) => colorCode.style.visibility = 'hidden');
        trackVisibility = false; 
      }
    }
  });
  