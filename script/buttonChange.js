"use strict";

  /**
  * @description Eventlistener to change the background color of the button
  * @param {event} e
  * @author Hugues Rouillard
  */
  document.querySelector("#color").addEventListener("change",function(e){
    let button = document.querySelector("#start-game-button");
    if(e.target.value === "red"){
      button.style.background = "red";
    } else if(e.target.value === "blue"){
      button.style.background = "blue";
      button.style.color = "white";
    } else if(e.target.value === "green"){
      button.style.background = "green";
      button.style.color = "white";
    }  
  });