"use strict";
  /**
  * @title changeBackgroudColor
  * @description Generates the colors for the background for an immersive experience
  * @param {number} diff
  * @author Vijay Patel
  */
function changeBackgroudColor(diff){
  let header = document.querySelector("header");
  let titleGameSetup = document.querySelector("#title-game-setup");
  let formInputs = document.querySelector("#form-inputs");
  let footer = document.querySelector("footer");
  let setup = document.querySelector("#setup-form");
  let arrayOfElements = [header,titleGameSetup,formInputs,footer,setup];

  arrayOfElements.forEach((x)=>{
    let color1 = generateRGBOne();
    let color2 = generateRBGTwo(color1,diff);
    let color3 = generateRBGThree(color1,color2,diff);

    x.style.background = `rgb(${color1},${color2},${color3})`
  });
}
  /**
  * @title checkDifficulty
  * @description Based on the difficulty set by the player, we generate a specific value that will be used in generating the colors
  * @param {event} e
  * @return {number}
  * @author Vijay Patel
  */
function checkDifficulty(e){
  if(e.target.difficulty.value === "0"){
    return 0;
  }
  else if(e.target.difficulty.value === "1"){
    return 80;
  }
  else if(e.target.difficulty.value === "2"){
    return 40;
  }
  else if(e.target.difficulty.value === "3"){
    return 10;
  }
}
  /**
  * @title highestRGB 
  * @description Determines which RGB value is the highest
  * @param {number} red
  * @param {number} green
  * @param {number} blue
  * @return {String}
  * @author Vijay Patel
  */
function highestRGB (red,green,blue){
  let highest = Math.max(red,green,blue);
  if(highest === red){
    return "red";
  } else if(highest === green){
    return "green";
  } else if(highest === blue){
    return "blue";
  }
}
  /**
  * @title colorSet
  * @description Generates the values for RGB, calls changeBackground() returns RGB String for cheat code
  * @param {String} chosenColor
  * @param {HTMLElement} td
  * @param {number} difficulty
  * @author Hugues Rouillard
  * @return {String[]}
  */
function colorSet(chosenColor,td,difficulty){
  let color1 = generateRGBOne();
  let color2 = generateRBGTwo(color1,difficulty);
  let color3 = generateRBGThree(color1,color2,difficulty);
  let RGB = null;
  let highestColor =null;
  if(chosenColor === "red"){
    RGB = `rgb(${color1},${color2},${color3})`;
    changeBackground(td,RGB);
    highestColor = highestRGB(color1,color2,color3);
  } else if(chosenColor === "green"){
    RGB = `rgb(${color2},${color1},${color3})`;
    changeBackground(td,RGB);
    highestColor = highestRGB(color2,color1,color3);
  } else if(chosenColor === "blue"){
    RGB = `rgb(${color2},${color3},${color1})`;
    changeBackground(td,RGB);
    highestColor = highestRGB(color2,color3,color1);
  }
  return [RGB, highestColor];
}
  /**
  * @title changeBackground
  * @description Changes the background for the td
  * @param {HTMLElement} td
  * @param {String} RGB
  * @author Vijay Patel 
  */
function changeBackground(td,RGB){
  td.style.background = RGB;
}
  /**
  * @title generateRGBOne
  * @description Generates the one of the three value for the RGB
  * @param {number} difficulty
  * @author Vijay Patel 
  * @return {number}
  */
  function generateRGBOne(){
    return Math.floor(Math.random() * 256);
  }
  /**
  * @title generateRBGTwo
  * @description Generates the second value for the RGB colors
  * @param {number} RGBONE
  * @param {number} difficulty
  * @author Vijay Patel 
  * @return {number}
  */
  function generateRBGTwo(RGBONE,difficulty){
    if(difficulty === 0){
      return generateRGBOne();
    }
    const min = Math.max(0, Math.ceil(RGBONE - difficulty));
    const max = Math.min(255, Math.floor(RGBONE + difficulty));

    return Math.floor(Math.random() * (max - min + 1) + min);
  }
  /**
  * @title generateRBGThree
  * @description Generates the last value for the RGB colors
  * @param {number} RGBONE
  * @param {number} RGBTWO
  * @param {number} difficulty
  * @author Vijay Patel 
  * @return {number}
  */
  function generateRBGThree(RGBONE,RGBTWO, difficulty){
    if(difficulty === 0){
      return generateRGBOne();
    }
    let small = Math.min(RGBONE,RGBTWO);
    let big = Math.max(RGBONE,RGBTWO);
    const min = Math.max(0,Math.ceil(big - difficulty));
    const max = Math.min(255,Math.floor(small + difficulty));

    return Math.floor(Math.random() * (max - min + 1) + min);
  }