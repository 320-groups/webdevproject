# WebDevProject

This is a color guessing game. 

## Getting started

To start the game, open index.html

## Name
Color guessing game

## Description
Game guessing game. Generate a table by entering correct values in the form and press generate. Once generated, select the tiles in which you think is the highest rgb value of the chosen color and press submit
## Authors and acknowledgment
Vijay Patel & Hugues Rouillard

## Project status
Completed
